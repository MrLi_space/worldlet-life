package com.worldlet.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author : Mr Li
 * @description : 认证中心主启动类
 * @since : 2021/12/17 3:29 下午
 */
@SpringBootApplication
public class WorldletAuthApplication {
    public static void main(String[] args) {
        SpringApplication.run(WorldletAuthApplication.class, args);
    }
}
