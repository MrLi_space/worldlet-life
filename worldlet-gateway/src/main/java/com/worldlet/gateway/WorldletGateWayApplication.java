package com.worldlet.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author : Mr Li
 * @description : 网关主启动类
 * @since : 2021/12/17 2:30 下午
 */
@SpringBootApplication
public class WorldletGateWayApplication {
    public static void main(String[] args) {
        SpringApplication.run(WorldletGateWayApplication.class, args);
    }
}
