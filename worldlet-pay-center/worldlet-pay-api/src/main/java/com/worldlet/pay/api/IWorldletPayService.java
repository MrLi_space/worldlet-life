package com.worldlet.pay.api;

import com.worldlet.common.core.result.R;
import com.worldlet.pay.fallback.WorldletPayFallBackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author : Mr Li
 * @description : 支付Feign外部接口
 * @since : 2021/9/29 2:48 下午
 */
@FeignClient(name = "iWorldletPayService",contextId = "worldlet-pay-server",fallbackFactory = WorldletPayFallBackFactory.class)
public interface IWorldletPayService {

    /**
     * 扫描并处理未完成支付的订单
     * @return 执行结果
     */
    @GetMapping("/processNotFinishPayOrder")
    R<String> processNotFinishPayOrder();
}
