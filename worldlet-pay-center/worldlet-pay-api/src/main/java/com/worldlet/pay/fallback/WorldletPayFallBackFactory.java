package com.worldlet.pay.fallback;

import com.worldlet.common.core.result.R;
import com.worldlet.common.core.result.RE;
import com.worldlet.pay.api.IWorldletPayService;
import feign.hystrix.FallbackFactory;

/**
 * @author : Mr Li
 * @description : Hystrix-降级工厂
 * @since : 2021/9/29 5:37 下午
 */
public class WorldletPayFallBackFactory implements FallbackFactory<IWorldletPayService> {

    @Override
    public IWorldletPayService create(Throwable throwable) {
        return () -> R.failed(RE.FAILED_TOO_MANY_REQUESTS);
    }
}
