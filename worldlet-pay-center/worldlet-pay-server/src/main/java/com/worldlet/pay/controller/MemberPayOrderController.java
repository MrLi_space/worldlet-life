/*
 * Copyright (c) 2021-2030 Worldlet.Life.Co.Ltd. All Rights Reserved.
 */
package com.worldlet.pay.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.netflix.ribbon.proxy.annotation.Hystrix;
import com.worldlet.pay.entity.MemberPayOrder;
import com.worldlet.pay.service.MemberPayOrderService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

/**
 * @author : Mr Li
 * @version : W1.0
 * @program : worldlet-pay-server
 * @description : 用户订单实体类 表控制层
 * @since : Created in 2021-12-10 16:51
 */
@RestController
@RequestMapping("memberPayOrderController")
public class MemberPayOrderController extends ApiController {
    /**
     * 服务对象
     */
    @Resource
    private MemberPayOrderService memberPayOrderService;

    /**
     * 分页查询所有数据
     *
     * @param page           分页对象
     * @param memberPayOrder 查询实体
     * @return 所有数据
     */
    @GetMapping
    public R selectAll(Page<MemberPayOrder> page, MemberPayOrder memberPayOrder) {
        return success(this.memberPayOrderService.page(page, new QueryWrapper<>(memberPayOrder)));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("{id}")
    public R selectOne(@PathVariable Serializable id) {
        return success(this.memberPayOrderService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param memberPayOrder 实体对象
     * @return 新增结果
     */
    @PostMapping
    public R insert(@RequestBody MemberPayOrder memberPayOrder) {
        return success(this.memberPayOrderService.save(memberPayOrder));
    }

    /**
     * 修改数据
     *
     * @param memberPayOrder 实体对象
     * @return 修改结果
     */
    @PutMapping
    @Hystrix
    public R update(@RequestBody MemberPayOrder memberPayOrder) {
        return success(this.memberPayOrderService.updateById(memberPayOrder));
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @DeleteMapping
    public R delete(@RequestParam("idList") List<Long> idList) {
        return success(this.memberPayOrderService.removeByIds(idList));
    }
}
