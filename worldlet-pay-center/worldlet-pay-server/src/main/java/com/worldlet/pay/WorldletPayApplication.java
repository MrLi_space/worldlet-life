package com.worldlet.pay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author : Mr Li
 * @description : 支付平台启动类
 * @since : 2021/9/29 5:52 下午
 */
@EnableFeignClients(basePackages = "com.worldlet.pay.api")
@SpringBootApplication
public class WorldletPayApplication {
    public static void main(String[] args) {
        SpringApplication.run(WorldletPayApplication.class,args);
    }


}
