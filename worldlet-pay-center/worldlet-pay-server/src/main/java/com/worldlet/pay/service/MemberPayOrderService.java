/*
 * Copyright (c) 2021-2030 Worldlet.Life.Co.Ltd. All Rights Reserved.
 */
package com.worldlet.pay.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.worldlet.pay.entity.MemberPayOrder;

/**
 * @author : Mr Li
 * @version : W1.0
 * @program : worldlet-pay-server
 * @description : 用户订单实体类 表服务接口
 * @since : Created in 2021-12-10 16:51
 */
public interface MemberPayOrderService extends IService<MemberPayOrder> {

}
