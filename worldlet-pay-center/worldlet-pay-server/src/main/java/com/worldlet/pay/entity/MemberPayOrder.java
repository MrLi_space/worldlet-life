/*
 * Copyright (c) 2021-2030 Worldlet.Life.Co.Ltd. All Rights Reserved.
 */
package com.worldlet.pay.entity;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Data;

/**
 * @author : Mr Li
 * @version : W1.0
 * @program : worldlet-pay-server
 * @description : 用户订单实体类 表实体类
 * @since : Created in 2021-12-10 16:51
 */
@Data
public class MemberPayOrder {

    /**
     * 主键id
     */
    private Long id;

    /**
     * 用户id
     */
    private Long memberId;

    /**
     * 订单号
     */
    private String orderNumber;

    /**
     * 订单金额
     */
    private BigDecimal orderAmount;

    /**
     * 实付金额
     */
    private BigDecimal realPayAmount;

    /**
     * 支付方式0微信扫码1APP微信2APP支付宝3支付宝扫码4数字货币5线下支付
     */
    private Integer payType;

    /**
     * 订单状态0待支付1支付成功2取消支付3已过期
     */
    private Integer payStatus;

    /**
     * 订单描述:给第三方支付提供的商品名称
     */
    private String orderDepict;

    /**
     * 支付系统订单号
     */
    private String transactionId;

    /**
     * 支付时间
     */
    private Date payTime;

    /**
     * 备注
     */
    private String remark;

    /**
     * 第三方支付地址/SDK所需交易串
     */
    private String payUrl;

    /**
     * 支付回调信息
     */
    private Object rollbackMessage;

    /**
     * 订单过期时间
     */
    private Date expirationTime;

    /**
     * 是否删除0未删除1已删除
     */
    private Integer isDelete;

    /**
     * 附件-线下支付截图
     */
    private String attachmentUrl;

    /**
     * 订单生成时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date modifyTime;
}
