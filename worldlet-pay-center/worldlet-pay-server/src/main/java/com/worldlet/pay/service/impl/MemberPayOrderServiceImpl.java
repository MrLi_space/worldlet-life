/*
 * Copyright (c) 2021-2030 Worldlet.Life.Co.Ltd. All Rights Reserved.
 */
package com.worldlet.pay.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.worldlet.pay.mapper.MemberPayOrderExtMapper;
import com.worldlet.pay.entity.MemberPayOrder;
import com.worldlet.pay.service.MemberPayOrderService;
import org.springframework.stereotype.Service;

/**
 * @author : Mr Li
 * @version : W1.0
 * @program : worldlet-pay-server
 * @description : 用户订单实体类 表服务实现类
 * @since : Created in 2021-12-10 16:51
 */
@Service("memberPayOrderService")
public class MemberPayOrderServiceImpl extends ServiceImpl<MemberPayOrderExtMapper, MemberPayOrder> implements MemberPayOrderService {

}
