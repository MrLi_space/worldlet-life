/*
 * Copyright (c) 2021-2030 Worldlet.Life.Co.Ltd. All Rights Reserved.
 */
package com.worldlet.pay.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.worldlet.pay.entity.MemberPayOrder;

/**
 * @author : Mr Li
 * @version : W1.0
 * @program : worldlet-pay-server
 * @description : 用户订单实体类 表数据库访问层
 * @since : Created in 2021-12-10 16:51
 */
public interface MemberPayOrderMapper extends BaseMapper<MemberPayOrder> {

}
