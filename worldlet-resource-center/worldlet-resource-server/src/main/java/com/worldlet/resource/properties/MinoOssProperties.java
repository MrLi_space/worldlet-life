package com.worldlet.resource.properties;

import com.worldlet.resource.exception.OssExceptionEnum;
import com.worldlet.resource.exception.WorldletOssException;
import io.minio.MinioClient;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author : Mr Li
 * @description : 阿里云配置
 * @link : java-sdk-doc - http://docs.minio.org.cn/docs/master/java-client-api-reference
 * @since : 2021/9/22 11:12 上午
 */
@Data
@Configuration
public class MinoOssProperties {

    @Value("${mino.oss.endpoint}")
    private String endpoint = "https://oss-cn-beijing.aliyuncs.com";

    @Value("${mino.oss.accessKeyId}")
    private String accessKeyId;

    @Value("${mino.oss.accessKeySecret}")
    private String accessKeySecret;

    @Value("${mino.oss.bucketName}")
    private String bucketName;

    @Bean
    public MinioClient minioClient() {
        try {
            return MinioClient.builder().endpoint(endpoint).credentials(accessKeyId, accessKeySecret).build();
        } catch (Exception e) {
            throw new WorldletOssException(OssExceptionEnum.OSS_CLIENT_INIT_ERROR, e.getMessage());
        }
    }
}
