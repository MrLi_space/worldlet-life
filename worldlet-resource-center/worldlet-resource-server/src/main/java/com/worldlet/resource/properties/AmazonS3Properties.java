package com.worldlet.resource.properties;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author : Mr Li
 * @description : 阿里云配置
 * @since : 2021/9/22 11:12 上午
 */
@Configuration
public class AmazonS3Properties {

    @Value("${s3.oss.endpoint}")
    private String endpoint = "https://oss-cn-beijing.aliyuncs.com";

    @Value("${s3.oss.accessKey}")
    private String accessKey;

    @Value("${s3.oss.accessKeySecret}")
    private String accessKeySecret;

    @Bean
    public AmazonS3 amazonS3() {
        ClientConfiguration config = new ClientConfiguration();

        AwsClientBuilder.EndpointConfiguration endpointConfig =
                new AwsClientBuilder.EndpointConfiguration(endpoint, "cn-north-1");

        AWSCredentials awsCredentials = new BasicAWSCredentials(accessKey, accessKeySecret);
        AWSCredentialsProvider awsCredentialsProvider = new AWSStaticCredentialsProvider(awsCredentials);

        return AmazonS3Client.builder()
                .withEndpointConfiguration(endpointConfig)
                .withClientConfiguration(config)
                .withCredentials(awsCredentialsProvider)
                .disableChunkedEncoding()
                .withPathStyleAccessEnabled(true)
                .build();

    }


}
