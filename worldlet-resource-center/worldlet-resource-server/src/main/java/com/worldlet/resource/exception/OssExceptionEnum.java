package com.worldlet.resource.exception;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author : Mr Li
 * @description : Oss-异常枚举
 * @since : 2021/9/22 2:33 下午
 */
@Slf4j
public enum OssExceptionEnum {

    INVALID_OSS_TYPE(500110, "无效的存储方式-%s", "Invalid storage method-%s"),

    OSS_CLIENT_INIT_ERROR(500111, "客户端初始化异常-%s", "Client initialization exception-%s"),

    OSS_CLIENT_CHECK_BUCKET_ERROR(500112, "%s存储服务器检测Bucket异常-%s", "%sStorage server detects Bucket abnormality-%s"),

    OSS_CLIENT_CREAT_BUCKET_ERROR(500113, "%s存储服务器创建Bucket异常-%s", "%s-Storage server creates a bucket abnormally-%s"),

    OSS_CLIENT_UPLOAD_FILE_ERROR(500114, "%s存储服务器上传文件异常-%s", "%sThe storage server uploads a file abnormally-%s"),

    FAILED_TO_GET_FILE_FROM_STORAGE_SERVER(500115, "%s存储服务器文件获取失败-%s", "%sStorage server file acquisition failed-%s"),
    ;

    @Getter
    private final int code;

    @Getter
    private final String msg;

    @Getter
    private final String englishMsg;

    OssExceptionEnum(int code, String msg, String englishMsg) {
        this.code = code;
        this.msg = msg;
        this.englishMsg = englishMsg;
    }

    public static OssExceptionEnum getInstance(int code) {
        for (OssExceptionEnum value : OssExceptionEnum.values()) {
            if (value.code == code)
                return value;
        }
        log.error("ERROR-无{}对应存储方式", code);
        throw new WorldletOssException(OssExceptionEnum.OSS_CLIENT_INIT_ERROR, code);
    }

}
