package com.worldlet.resource.factory;

import com.worldlet.resource.exception.OssClientEnum;

import java.io.File;

/**
 * @author : Mr Li
 * @description : Oss文件操作接口
 * @since : 2021/9/22 1:53 下午
 */
public interface OssClientInterface {

    OssClientEnum getProperty();

    /**
     * 检测是否存在bucketName
     *
     * @param bucketName bucket名称
     * @return bucket是否存在 存在=>t
     */
    boolean doesBucketExist(String bucketName);

    /**
     * 创建bucketName
     *
     * @param bucketName bucket名称
     */
    void creatBucket(String bucketName);

    /**
     * 上传文件
     *
     * @param bucketName bucketName
     * @param firstKey   文件存储在服务器的文件名称
     * @param file       文件
     * @return 存储结果
     */
    default String putObject(String bucketName, String firstKey, File file) {
        if (!doesBucketExist(bucketName)) {
            creatBucket(bucketName);
        }
        return putFileObject(bucketName, firstKey, file);
    }

    /**
     * 上传文件
     *
     * @param bucketName bucketName
     * @param firstKey   文件存储在服务器的文件名称
     * @param file       文件
     * @return 存储地址
     */
    String putFileObject(String bucketName, String firstKey, File file);

    /**
     * 删除指定名称的文件
     *
     * @param bucketName bucketName
     * @param firstKey   文件存储在服务器的文件名称
     */
    void removeObject(String bucketName, String firstKey);


}
