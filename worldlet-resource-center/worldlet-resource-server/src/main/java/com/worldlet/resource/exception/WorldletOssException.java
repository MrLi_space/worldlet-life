package com.worldlet.resource.exception;

import com.worldlet.common.core.exception.WorldLetException;
import com.worldlet.common.core.result.RE;

/**
 * @author : Mr Li
 * @description : OSS-异常
 * @since : 2021/9/22 2:30 下午
 */
public class WorldletOssException extends WorldLetException {

    public WorldletOssException(RE re) {
        super(re);
    }

    public WorldletOssException(OssExceptionEnum ossExceptionEnum) {
        super(ossExceptionEnum.getCode(),ossExceptionEnum.getMsg(),ossExceptionEnum.getEnglishMsg());
    }

    public WorldletOssException(OssExceptionEnum ossExceptionEnum,Object... parameter) {
        super(ossExceptionEnum.getCode(),String.format(ossExceptionEnum.getMsg(),parameter),String.format(ossExceptionEnum.getEnglishMsg(),parameter));
    }

}
