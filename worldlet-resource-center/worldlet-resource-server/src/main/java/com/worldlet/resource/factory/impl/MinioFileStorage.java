package com.worldlet.resource.factory.impl;

import cn.hutool.core.text.StrPool;
import com.worldlet.resource.exception.OssClientEnum;
import com.worldlet.resource.exception.OssExceptionEnum;
import com.worldlet.resource.exception.WorldletOssException;
import com.worldlet.resource.factory.OssClientInterface;
import com.worldlet.resource.properties.MinoOssProperties;
import io.minio.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.File;

/**
 * @author : Mr Li
 * @description : Mino文件存储器
 * @since : 2021/9/22 1:32 下午
 */
@Slf4j
@Component
public class MinioFileStorage implements OssClientInterface {

    @Autowired
    private MinioClient minioClient;

    @Resource
    private MinoOssProperties minoOssProperties;

    @Override
    public OssClientEnum getProperty() {
        return OssClientEnum.MINO_OSS;
    }

    @Override
    public boolean doesBucketExist(String bucketName) {
        boolean result;
        try {
            result = minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build());
        } catch (Exception e) {
            throw new WorldletOssException(OssExceptionEnum.OSS_CLIENT_CHECK_BUCKET_ERROR, e.getMessage());
        }
        return result;
    }

    @Override
    public void creatBucket(String bucketName) {
        try {
            minioClient.makeBucket(MakeBucketArgs.builder().bucket(bucketName).build());
        } catch (Exception e) {
            throw new WorldletOssException(OssExceptionEnum.OSS_CLIENT_CREAT_BUCKET_ERROR, e.getMessage());
        }
    }

    @Override
    public String putFileObject(String bucketName, String firstKey, File file) {
        try {
            minioClient.uploadObject(UploadObjectArgs.builder().bucket(bucketName).object(firstKey).filename(file.getName()).build());
            return minoOssProperties.getEndpoint() + StrPool.SLASH + bucketName + StrPool.SLASH + firstKey;
        } catch (Exception e) {
            throw new WorldletOssException(OssExceptionEnum.OSS_CLIENT_UPLOAD_FILE_ERROR, e.getMessage());
        }
    }

    @Override
    public void removeObject(String bucketName, String firstKey) {
        try {
            minioClient.removeObject(RemoveObjectArgs.builder().bucket(bucketName).object(firstKey).build());
        } catch (Exception e) {
            throw new WorldletOssException(OssExceptionEnum.OSS_CLIENT_UPLOAD_FILE_ERROR, e.getMessage());
        }
    }

}
