package com.worldlet.resource.exception;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author : Mr Li
 * @description : OSS-存储客户端枚举
 * @since : 2021/9/22 1:37 下午
 */
@Slf4j
public enum OssClientEnum {

    ALI_OSS(0, "阿里云Oss存储"),
    MINO_OSS(1, "MinioOss存储"),
    AMAZON_S3_OSS(2, "亚马逊S3存储");

    @Getter
    private final int type;

    @Getter
    private final String ossTypeName;

    OssClientEnum(int type, String ossTypeName) {
        this.type = type;
        this.ossTypeName = ossTypeName;
    }

    public static OssClientEnum getInstance(int type) {
        for (OssClientEnum value : OssClientEnum.values()) {
            if (value.type == type)
                return value;
        }
        log.error("ERROR-无{}对应存储方式", type);
        throw new WorldletOssException(OssExceptionEnum.INVALID_OSS_TYPE, type);
    }
}
