package com.worldlet.resource.factory.impl;

import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.worldlet.resource.exception.OssClientEnum;
import com.worldlet.resource.exception.OssExceptionEnum;
import com.worldlet.resource.exception.WorldletOssException;
import com.worldlet.resource.factory.OssClientInterface;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.Objects;

/**
 * @author : Mr Li
 * @description : AliYunOss
 * @since : 2021/9/8 10:24 上午
 */
@Slf4j
@Component
public class AmazonS3FileStorage implements OssClientInterface {

    @Autowired
    private AmazonS3 amazonS3;

    @Override
    public OssClientEnum getProperty() {
        return OssClientEnum.AMAZON_S3_OSS;
    }

    @Override
    public boolean doesBucketExist(String bucketName) {
        return amazonS3.doesBucketExistV2(bucketName);
    }

    @Override
    public void creatBucket(String bucketName) {
        try {
            amazonS3.createBucket(bucketName);
        } catch (Exception e) {
            throw new WorldletOssException(OssExceptionEnum.OSS_CLIENT_CREAT_BUCKET_ERROR, e.getMessage());
        }
    }

    @Override
    public String putFileObject(String bucketName, String firstKey, File file) {
        try {
            final PutObjectResult putObjectResult = amazonS3.putObject(bucketName, firstKey, file);
            if (Objects.nonNull(putObjectResult)) {
                return amazonS3.getUrl(bucketName, firstKey).getRef();
            }
            throw new WorldletOssException(OssExceptionEnum.OSS_CLIENT_UPLOAD_FILE_ERROR);
        } catch (SdkClientException e) {
            throw new WorldletOssException(OssExceptionEnum.OSS_CLIENT_UPLOAD_FILE_ERROR, e.getMessage());
        }
    }

    @Override
    public void removeObject(String bucketName, String key) {
        amazonS3.deleteObject(bucketName, key);
    }


}
