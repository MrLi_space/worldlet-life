package com.worldlet.resource.factory.impl;

import cn.hutool.http.HttpStatus;
import com.aliyun.oss.OSS;
import com.aliyun.oss.common.comm.ResponseMessage;
import com.worldlet.resource.exception.OssClientEnum;
import com.worldlet.resource.exception.OssExceptionEnum;
import com.worldlet.resource.exception.WorldletOssException;
import com.worldlet.resource.factory.OssClientInterface;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;

/**
 * @author : Mr Li
 * @description : AliYunOss
 * @since : 2021/9/8 10:24 上午
 */
@Slf4j
@Component
public class ALiYunFileStorage implements OssClientInterface {

    @Autowired
    private OSS aLiYunOSSClient;

    @Override
    public OssClientEnum getProperty() {
        return OssClientEnum.ALI_OSS;
    }

    @Override
    public boolean doesBucketExist(String bucketName) {
        return aLiYunOSSClient.doesBucketExist(bucketName);
    }

    @Override
    public void creatBucket(String bucketName) {
        try {
            aLiYunOSSClient.createBucket(bucketName);
        } catch (Exception e) {
            throw new WorldletOssException(OssExceptionEnum.OSS_CLIENT_CREAT_BUCKET_ERROR, e.getMessage());
        }
    }

    @Override
    public String putFileObject(String bucketName, String firstKey, File file) {
        final ResponseMessage response = aLiYunOSSClient.putObject(bucketName, firstKey, file).getResponse();
        if (HttpStatus.HTTP_OK == response.getStatusCode()) {
            return response.getUri();
        }
        throw new WorldletOssException(OssExceptionEnum.OSS_CLIENT_UPLOAD_FILE_ERROR, response.getErrorResponseAsString());
    }

    @Override
    public void removeObject(String bucketName, String key) {
        aLiYunOSSClient.deleteObject(bucketName, key);
    }


}
