package com.worldlet.resource.factory;

import com.worldlet.resource.exception.OssClientEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author : Mr Li
 * @description : 存储器抽象工厂
 * @since : 2021/9/22 1:48 下午
 */
@Slf4j
@Component
public class OssClientFactory {

    private static final ConcurrentHashMap<OssClientEnum, OssClientInterface> enumConcurrentHashMap = new ConcurrentHashMap<>();

    @Autowired
    public OssClientFactory(List<OssClientInterface> clients){
        clients.forEach(c->enumConcurrentHashMap.put(c.getProperty(),c));
        log.info("Oss客户端加载完成{}",clients);
    }

    public OssClientInterface getInstance(int type){
        return enumConcurrentHashMap.get(OssClientEnum.getInstance(type));
    }
}
