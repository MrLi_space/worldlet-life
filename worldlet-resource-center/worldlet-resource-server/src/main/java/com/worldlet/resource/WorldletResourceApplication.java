package com.worldlet.resource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author : Mr Li
 * @description : 资源服务器启动类
 * @since : 2021/12/16 10:59 上午
 */
@SpringBootApplication
public class WorldletResourceApplication {

    public static void main(String[] args) {
        SpringApplication.run(WorldletResourceApplication.class, args);
    }

}
