package com.worldlet.common.redisson.annotation;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * <h3>防止用户同一时间点重复提交相同的数据,导致不同操作数据所带来的不可逆转的影响<h3/>
 * Lock Target   : token+requestPath
 * <p>
 * lockTime      : 锁时间大小 默认 30
 * <p>
 * timeUnit      : 锁时间单位 默认 SECONDS
 * <p>
 * <h3>Aspect「AOP」<h3/>
 *
 * @author : Mr Li
 * @see com.worldlet.common.redisson.aspect.UserRequestLockAspect
 * 举例 For example:
 * <p>
 * &#064;RequestLock({lockTime=10,timeUnit=TimeUnit.SECONDS})
 * public void updateMoney()
 * {
 * //业务逻辑
 * }
 * @since : 2021/11/19 2:02 下午
 */
@Target(value = ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface UserRequestLock {

    /**
     * 锁时间大小 默认30
     */
    int lockTime() default 30;

    /**
     * 时间单位 {@link java.util.concurrent.TimeUnit}
     */
    TimeUnit timeUnit() default TimeUnit.SECONDS;

}
