package com.worldlet.common.redisson.config;

import cn.hutool.core.text.StrPool;
import lombok.extern.slf4j.Slf4j;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.util.Arrays;

/**
 * @author : Mr Li
 * @description : redisson集群配置
 * @since : 2021/8/21 8:14 下午
 */
@Slf4j
@Configuration
public class RedissonClusterConfig {

    @Value("${spring.redis.cluster.nodes}")
    private String clusterNodes;
    @Value("${spring.redis.password}")
    private String password;

    /*添加redisson的bean */
    @Bean
    public Redisson clusterRedisson() {
        /*redisson版本是3.5，集群的ip前面要加上“redis://”，不然会报错，3.2版本可不加 */
        Config config = new Config();
        RedissonClient redisson = null;
        var cluster = Arrays.asList(clusterNodes.split(StrPool.COMMA));
        cluster.forEach(c->c="redis://"+c);
        try {
            config.useClusterServers()
                    .setPassword(password)
                    .setCheckSlotsCoverage(false)//取消检查插槽覆盖率
                    .setScanInterval(200000)//设置集群状态扫描间隔
                    .setMasterConnectionPoolSize(10000)//设置对于master节点的连接池中连接数最大为10000
                    .setSlaveConnectionPoolSize(500)//设置对于slave节点的连接池中连接数最大为500
                    .setIdleConnectionTimeout(10000)//如果当前连接池里的连接数量超过了最小空闲连接数，而同时有连接空闲时间超过了该数值，那么这些连接将会自动被关闭，并从连接池里去掉。时间单位是毫秒。
                    .setConnectTimeout(30000)//同任何节点建立连接时的等待超时。时间单位是毫秒。
                    .setTimeout(3000)//等待节点回复命令的时间。该时间从命令发送成功时开始计时。
                    .setRetryInterval(3000)//当与某个节点的连接断开时，等待与其重新建立连接的时间间隔。时间单位是毫秒。
                    .setNodeAddresses(cluster);
            redisson = Redisson.create(config);
        } catch (Exception e) {
            log.error("集群初始化配置:{}",e.getMessage());
        }
        return (Redisson) redisson;
    }
}