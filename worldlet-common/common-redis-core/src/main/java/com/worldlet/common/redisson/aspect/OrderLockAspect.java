package com.worldlet.common.redisson.aspect;

import cn.hutool.core.text.CharSequenceUtil;
import com.worldlet.common.core.enums.WorldletConstantEnum;
import com.worldlet.common.core.result.R;
import com.worldlet.common.core.result.RE;
import com.worldlet.common.core.util.HttpRequestUtil;
import com.worldlet.common.redisson.annotation.OrderLock;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.redisson.Redisson;
import org.redisson.api.RLock;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.Objects;

import static com.worldlet.common.redisson.aspect.CommonLockAspect.pointLockCommonCode;

/**
 * @author : Mr Li
 * @description : 订单锁AOP
 * @since : 2021/11/19 2:04 下午
 */
@Aspect
@Component
public class OrderLockAspect {

    @Resource
    private Redisson redisson;

    @Pointcut("@annotation(orderLock)")
    public void pointcut(OrderLock orderLock) {
        /* Implant cut point - 植入切点 */
    }

    @Around(value = "pointcut(orderLock)", argNames = "proceedingJoinPoint,orderLock")
    public Object around(ProceedingJoinPoint proceedingJoinPoint, OrderLock orderLock) {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (Objects.nonNull(requestAttributes)) {
            /* 解析 HttpServletRequest 请求体 Body , 转为Map类型 */
            HttpServletRequest request = requestAttributes.getRequest();
            Map<String, Object> requestMap = HttpRequestUtil.convertBodyToMap(request);
            final String orderNumber = String.valueOf(requestMap.get(WorldletConstantEnum.ORDER_NUMBER.getFiledName()));
            /* 判断订单号 isNotBlank */
            if (CharSequenceUtil.isNotBlank(orderNumber)) {
                /* 获取锁状态 */
                final RLock lock = redisson.getLock(orderNumber);
                return pointLockCommonCode(proceedingJoinPoint, lock, orderLock.lockTime(), orderLock.timeUnit());
            }
            return R.failed(RE.FAILED_REPEAT_PARAM_EMPTY, WorldletConstantEnum.ORDER_NUMBER.getFiledName());
        }
        return R.failed(RE.FAILED);
    }

}
