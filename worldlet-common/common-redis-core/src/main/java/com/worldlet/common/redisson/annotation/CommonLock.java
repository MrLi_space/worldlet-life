package com.worldlet.common.redisson.annotation;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * <h3>常见锁<h3/>
 * Lock Target   : 商品库存、用户资金等常见数额
 * <p>
 * lockTime      : 锁时间大小 默认 30
 * <p>
 * timeUnit      : 锁时间单位 默认 SECONDS
 * <p>
 * filedName     : 字段名称   默认 userId
 * <p>
 * filedType     : 字段类型   默认 String
 * <p>
 * <h3>Aspect「AOP」<h3/>
 *
 * @author : Mr Li
 * @see com.worldlet.common.redisson.aspect.CommonLockAspect
 * 举例 For example:
 * <p>
 * &#064;RequestLock({lockTime=10,timeUnit=TimeUnit.SECONDS})
 * public void updateMoney()
 * {
 * //业务逻辑
 * }
 * @since : 2021/11/19 2:02 下午
 */
@Target(value = ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CommonLock {

    /**
     * 锁时间大小 默认30
     */
    int lockTime() default 30;

    /**
     * 时间单位 {@link java.util.concurrent.TimeUnit}
     */
    TimeUnit timeUnit() default TimeUnit.SECONDS;

    /**
     * 字段名称
     */
    String fieldName() default "userId";

    /**
     * 字段类型
     */
    Class<?> fieldType() default Long.class;

}
