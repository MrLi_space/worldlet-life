package com.worldlet.common.redisson.aspect;

import cn.hutool.core.text.CharSequenceUtil;
import com.worldlet.common.core.util.WebUtil;
import com.worldlet.common.redisson.annotation.UserRequestLock;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.redisson.Redisson;
import org.redisson.api.RLock;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import static com.worldlet.common.redisson.aspect.CommonLockAspect.pointLockCommonCode;

/**
 * @author : Mr Li
 * @description : 用户请求路径锁AOP
 * @since : 2021/11/22 4:23 下午
 */
@Aspect
@Component
public class UserRequestLockAspect {

    @Resource
    private Redisson redisson;

    @Pointcut("@annotation(userRequestLock)")
    public void pointCut(UserRequestLock userRequestLock) {
        /* Implant cut point - 植入切点 */
    }

    @Around(value = "pointCut(userRequestLock)", argNames = "pjp,userRequestLock")
    public Object around(ProceedingJoinPoint pjp, UserRequestLock userRequestLock) {
        HttpServletRequest request = WebUtil.getRequest();
        String bearerToken = request.getHeader(HttpHeaders.AUTHORIZATION);
        String[] tokens = bearerToken.split(CharSequenceUtil.SPACE);
        String token = tokens[1];
        String path = request.getServletPath();
        String key = token + path;
        final RLock lock = redisson.getLock(key);
        return pointLockCommonCode(pjp, lock, userRequestLock.lockTime(), userRequestLock.timeUnit());
    }

}
