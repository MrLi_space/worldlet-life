//package com.worldlet.common.redisson.config;
//
//import lombok.Data;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//
///**
// * @author : Li Hui
// * @description : redis配置
// * @since : 2021/6/29 2:30 下午
// */
//@Data
//@ConfigurationProperties(prefix = "redisson")
//public class RedissonProperties {
//    /**
//     * 地址
//     */
//    private String address;
//    /**
//     * 密码
//     */
//    private String password;
//
//    /**
//     * 库
//     */
//    private int database = 0;
//
//    /**
//     * 超时时间
//     */
//    private int timeout = 4000;
//
//    /**
//     * 连接超时时间
//     */
//    private int connectTimeout = 10000;
//
//}
/*package com.worldlet.common.redisson.config;
//
//import lombok.extern.slf4j.Slf4j;
//import org.redisson.Redisson;
//import org.redisson.api.RedissonClient;
//import org.redisson.config.Config;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
//import org.springframework.boot.context.properties.EnableConfigurationProperties;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
///**
// * @author : Li Hui
// * @description : redisson配置
// * @since : 2021/6/29 2:30 下午
// */
//@Slf4j
//@Configuration
//@ConditionalOnClass(Redisson.class)
//@EnableConfigurationProperties(RedissonProperties.class)
//public class RedissonClusterConfig {
//    private final RedissonProperties properties;
//
//    public RedissonClusterConfig(RedissonProperties properties) {
//        this.properties = properties;
//        log.info("RedissonConfig 加载成功 redis address: {}, database: {}", properties.getAddress(), properties.getDatabase());
//    }
//
//    /**
//     * @return RedissonClient
//     */
//    @Bean(name = "redissonClient")
//    public RedissonClient redissonClient() {
//        Config config = new Config();
//        config.useSingleServer()
//                .setAddress(properties.getAddress())
//                .setDatabase(properties.getDatabase())
//                .setPassword(properties.getPassword())
//                .setTimeout(properties.getTimeout())
//                .setConnectTimeout(properties.getConnectTimeout());
//        return Redisson.create(config);
//    }
//}
//
