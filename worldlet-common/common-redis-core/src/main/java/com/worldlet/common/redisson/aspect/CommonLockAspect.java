package com.worldlet.common.redisson.aspect;

import com.worldlet.common.core.result.R;
import com.worldlet.common.core.result.RE;
import com.worldlet.common.core.util.HttpRequestUtil;
import com.worldlet.common.redisson.annotation.CommonLock;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.redisson.Redisson;
import org.redisson.api.RLock;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * @author : Mr Li
 * @description : 订单锁AOP
 * @since : 2021/11/19 2:04 下午
 */
@Aspect
@Component
public class CommonLockAspect {

    @Resource
    private Redisson redisson;

    @Pointcut("@annotation(commonLock)")
    public void pointcut(CommonLock commonLock) {
        /* Implant cut point - 植入切点 */
    }

    @Around(value = "pointcut(commonLock)", argNames = "proceedingJoinPoint,commonLock")
    public Object around(ProceedingJoinPoint proceedingJoinPoint, CommonLock commonLock) {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (Objects.nonNull(requestAttributes)) {
            /* 解析 HttpServletRequest 请求体 Body , 转为Map类型 */
            HttpServletRequest request = requestAttributes.getRequest();
            Map<String, Object> requestMap = HttpRequestUtil.convertBodyToMap(request);
            final Object o = requestMap.get(commonLock.fieldName());
            if (Objects.isNull(o)) {
                return R.failed(RE.FAILED_REPEAT_PARAM_EMPTY, commonLock.fieldName());
            }
            final RLock lock = redisson.getLock(String.valueOf(o));
            return pointLockCommonCode(proceedingJoinPoint, lock, commonLock.lockTime(), commonLock.timeUnit());
        }
        return R.failed(RE.FAILED);
    }

    /**
     * 切点加锁公共代码
     *
     * @param proceedingJoinPoint 切点对象
     * @param lock                锁对象
     * @param lockTime            加锁时间
     * @param timeUnit            时间单位
     * @return 执行结果
     */
    public static Object pointLockCommonCode(ProceedingJoinPoint proceedingJoinPoint, RLock lock, int lockTime, TimeUnit timeUnit) {
        if (Boolean.TRUE.equals(lock.isLocked())) {
            try {
                lock.lock(lockTime, timeUnit);
                return proceedingJoinPoint.proceed();
            } catch (Throwable throwable) {
                throwable.printStackTrace();
                return R.failed(RE.FAILED);
            } finally {
                lock.unlock();
            }
        }
        return R.failed(RE.FAILED_REPEAT_REQUEST);
    }

}
