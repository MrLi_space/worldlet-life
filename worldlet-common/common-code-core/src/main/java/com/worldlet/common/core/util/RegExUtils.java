package com.worldlet.common.core.util;


import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author : Li Hui
 * @description : 正则表达式匹配两个字符串之间的内容
 * @since : 2021/9/22 10:14 上午
 */
public class RegExUtils extends org.apache.commons.lang3.RegExUtils {

    private RegExUtils() {
    }

    public static String convertProductDescription(String str) {
        StringBuilder sb = new StringBuilder();
        List<String> imgArr = Arrays.asList(str.split("p><img"));
        for (int i = 0; i < imgArr.size(); i++) {
            String img = imgArr.get(i);
            if (i != imgArr.size() - 1) {
                img = img + "p><img";
            }
            if (img.contains("src")) {
                if (img.contains("style=")) {
                    String regex = "style=\"(.*?)\"";
                    String regexStr = getSubUtilSimple(img, regex);
                    if (!regexStr.contains("max-width: 100%;")) {
                        img = img.replace(regexStr, regexStr + "max-width: 100%");
                    }
                } else {
                    img = " style=\"max-width: 100%;\"" + img;
                }
            }
            sb.append(img);
        }
        return sb.toString();
    }

    /**
     * 返回单个字符串，若匹配到多个的话就返回第一个，方法与getSubUtil一样
     *
     * @param soap  s
     * @param regex r
     * @return s
     */
    private static String getSubUtilSimple(String soap, String regex) {
        // 匹配的模式
        Pattern pattern = Pattern.compile(regex);
        Matcher m = pattern.matcher(soap);
        if (m.find()) {
            return m.group(1);
        }
        return "";
    }
}