package com.worldlet.common.core.util;

import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;


/**
 * @author : Li Hui
 * @description : rsa 解密
 * @since : 2021/9/22 10:14 上午
 */
@Component
@ConditionalOnProperty(name = "rsa.private_key")
public class RsaUtil {

    @Value("${rsa.private_key}")
    private String privateKey;

    public String decoder(String password) {
        return new String(new RSA(privateKey, null).decrypt(password, KeyType.PrivateKey));
    }
}