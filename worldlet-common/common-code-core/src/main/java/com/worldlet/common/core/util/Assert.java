package com.worldlet.common.core.util;

import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.core.text.StrPool;
import cn.hutool.core.util.ObjectUtil;
import com.worldlet.common.core.exception.WorldLetException;
import com.worldlet.common.core.result.RE;
import java.util.Objects;

/**
 * @author : Li Hui
 * @description : 断言
 * @since : 2021/9/22 10:14 上午
 */
public class Assert {
    private Assert() {
    }

    /**
     * 指定数据不存在
     * 多在 编辑,删除 通过id 操作数据 未获取到值
     *
     * @param object 对象
     */
    public static void isEmpty(Object object) {
        if (ObjectUtil.isEmpty(object)) {
            throw new WorldLetException(RE.FAILED_EMPTY);
        }
    }

    /**
     * 请求参数为空
     * 多在 参数校验用
     *
     * @param requestParam 请求参数
     */
    public static void reqParamEmpty(Object... requestParam) {
        if (ObjectUtil.isEmpty(requestParam)) {
            throw new WorldLetException(RE.FAILED_REPEAT_PARAM_EMPTY, requestParam);
        }
    }


    /**
     * 参数有效性校验
     *
     * @param param  入参
     * @param target 可选值
     */
    public static void validParam(String param, String... target) {
        if (!CharSequenceUtil.equalsAny(param, target)) {
            throw new WorldLetException(RE.FAILED_REPEAT_PARAM_INVALID,String.join(StrPool.COMMA, param),String.join(StrPool.COMMA, target));
        }
    }

    /**
     * 校验参数范围 包含max，min
     *
     * @param value 值
     * @param max   最大值
     * @param min   最小值
     */
    public static void paramContainRange(String parameter ,Integer value, Integer min, Integer max) {
        if (Objects.isNull(value)) {
            throw new WorldLetException(RE.FAILED_REPEAT_PARAM_EMPTY,parameter);
        }
        if (value > max || value < min) {
            throw new WorldLetException(RE.FAILED_REQUEST_MIN_MAX,parameter,min,max);
        }
    }
}