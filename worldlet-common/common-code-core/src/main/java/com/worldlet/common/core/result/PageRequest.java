package com.worldlet.common.core.result;

import cn.hutool.db.sql.Direction;
import lombok.Data;

/**
 * @author : Li Hui
 * @description : 全局分页参数请求体
 * @since : 2021/11/16 10:14 上午
 */
@Data
public abstract class PageRequest {

    private static final Integer DEFAULT_PAGE = 1;
    private static final Integer DEFAULT_SIZE = 10;
    /**
     * Id
     */
    private Long id;

    /**
     * 页码，不传默认为1
     */
    private Integer page = DEFAULT_PAGE;

    /**
     * 每页个数，不传默认为10
     */
    private Integer size = DEFAULT_SIZE;

    /**
     * 开始时间，格式：yyyy-MM-dd
     */
    private String startDate;
    /**
     * 结束时间，格式：yyyy-MM-dd
     */
    private String endDate;

    /**
     * 排序字段类型
     *
     * @description 自身实现类里重写获取排序字段方法 采用Integer防止String类型的参数导致Sql注入
     */
    private Integer sortType;

    /**
     * 排序格式： 0 降序 ASC 、1 升序 DESC
     */
    private Integer orderBy;

    public void setSize(Integer size) {
        if (size == null) {
            this.size = DEFAULT_SIZE;
        } else if (size < 1) {
            this.size = 1;
        } else {
            this.size = size;
        }
    }

    public String getSortName(Integer sortType) {
        return PageEnum.getInstance(sortType).getName();
    }

    public String getOrderByName(Integer orderBy) {
        if (orderBy != null && orderBy == 0) {
            return Direction.ASC.name();
        }
        return Direction.DESC.name();
    }

}
