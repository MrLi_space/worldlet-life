package com.worldlet.common.core.util;

import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.core.util.StrUtil;

/**
 * @author : Li Hui
 * @description : 正则表达式匹配两个字符串之间的内容
 * @since : 2021/9/22 10:14 上午
 */
public class StrUtils extends cn.hutool.core.util.StrUtil {

    private StrUtils() {
    }

    /**
     * 去掉两头 ,
     *
     * @param srcStr 要替换的字符串
     * @return 替换后的字符串
     */
    public static String trimBothEndsChars(String srcStr) {
        String regex = "^,*|,*$";
        return srcStr.replaceAll(regex, CharSequenceUtil.EMPTY);
    }

}