package com.worldlet.common.core.enums;

/**
 * @author : Mr Li
 * @description : 系统常量
 * @since : 2021/11/19 2:38 下午
 */
public enum WorldletConstantEnum {

    ORDER_NUMBER("orderNumber", "订单号"),
    USER_ID("userId", "用户id"),
    ;

    private final String filedName;

    private final String filedType;

    WorldletConstantEnum(String filedName, String filedType) {
        this.filedName = filedName;
        this.filedType = filedType;
    }

    public String getFiledName() {
        return filedName;
    }

    public String getFiledType() {
        return filedType;
    }

}
