package com.worldlet.common.core.result;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author : Mr Li
 * @description : 请求VO
 * @since : 2021/11/16 3:18 下午
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class MemberVO extends PageRequest{

    /**
     * 主键id
     */
    private Long id;

    @Override
    public String getSortName(Integer sortType) {
        return PageEnum.getInstance(sortType).getName();
    }
}
