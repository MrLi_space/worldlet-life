package com.worldlet.common.core.result;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.worldlet.common.core.constant.WorldletGlobalConstant;
import com.worldlet.common.core.enums.WorldletLanguageEnum;
import com.worldlet.common.core.util.WebUtil;
import lombok.Data;

import java.io.Serializable;

/**
 * @author : Li Hui
 * @description : 全局范型返回值
 * @since : 2021/9/22 10:14 上午
 */
@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class R<T> implements Serializable {
    private static final long serialVersionUID = 1L;


    private int code;

    private String msg;

    private T data;

    public static <T> R<T> ok() {
        return restResult(null, RE.OK);
    }

    public static <T> R<T> ok(T data) {
        return restResult(data, RE.OK);
    }

    public static <T> R<T> ok(T data, String msg, String englishMsg) {
        return restResult(data, RE.OK.getCode(), msg, englishMsg);
    }

    public static <T> R<T> failed() {
        return restResult(null, RE.FAILED);
    }

    public static <T> R<T> failed(int code, String msg) {
        return restResult(null, code, msg);
    }

    public static <T> R<T> failed(int code, String msg, String englishMsg) {
        return restResult(null, code, msg, englishMsg);
    }

    public static <T> R<T> failed(RE re) {
        return restResult(null, re);
    }

    public static <T> R<T> failed(RE re, Object... parameters) {
        final String msg = WorldletLanguageEnum.getMsg(WebUtil.getRequest().getHeader(WorldletGlobalConstant.LANGUAGE), re, parameters);
        return restResult(null, re.getCode(), msg);
    }

    private static <T> R<T> restResult(T data, RE re) {
        R<T> result = new R<>();
        result.setCode(re.getCode());
        result.setData(data);
        final String msg = WorldletLanguageEnum.getMsg(WebUtil.getRequest().getHeader(WorldletGlobalConstant.LANGUAGE), re);
        result.setMsg(msg);
        return result;
    }

    private static <T> R<T> restResult(T data, int code, String msg) {
        R<T> result = new R<>();
        result.setCode(code);
        result.setData(data);
        result.setMsg(msg);
        return result;
    }

    private static <T> R<T> restResult(T data, int code, String msg, String englishMsg) {
        R<T> result = new R<>();
        result.setCode(code);
        result.setData(data);
        final String resultMsg = WorldletLanguageEnum.getMsg(WebUtil.getRequest().getHeader(WorldletGlobalConstant.LANGUAGE), msg, englishMsg);
        result.setMsg(resultMsg);
        return result;
    }
}