package com.worldlet.common.core.util;

import cn.hutool.core.codec.Base64;
import com.worldlet.common.core.exception.WorldLetException;
import com.google.common.net.HttpHeaders;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

/**
 * @author : Li Hui
 * @description : 获取请求头参数
 * @since : 2021/9/22 10:14 上午
 */
@Slf4j
public class WebUtil extends org.springframework.web.util.WebUtils {
    private static final String BASIC = "Basic ";

    private WebUtil(){}

    /**
     * 获取 HttpServletResponse
     *
     * @return {HttpServletResponse}
     */
    public static HttpServletResponse getResponse() {
        return ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getResponse();
    }


    /**
     * HttpServletRequest
     *
     * @return HttpServletRequest
     */
    public static HttpServletRequest getRequest() {
        return ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
    }

    /**
     * 从request 获取CLIENT_ID
     *
     * @return clientId
     */
    @SneakyThrows
    public static String getClientId(ServerHttpRequest request) {
        String header = request.getHeaders().getFirst(HttpHeaders.AUTHORIZATION);
        return getClientId(header);
    }

    @SneakyThrows
    private static String getClientId(String header) {
        if (header == null || !header.startsWith(BASIC)) {
            throw new WorldLetException("请求头中client信息为空","The client information in the request header is empty");
        }
        byte[] base64Token = header.substring(6).getBytes(StandardCharsets.UTF_8);
        byte[] decoded;
        try {
            decoded = Base64.decode(base64Token);
        } catch (IllegalArgumentException e) {
            throw new WorldLetException(
                    "无法解码基本身份验证令牌","Failed to decode basic authentication token");
        }
        String token = new String(decoded, StandardCharsets.UTF_8);
        int delim = token.indexOf(":");
        if (delim == -1) {
            throw new WorldLetException("无效的身份令牌","Invalid basic authentication token");
        }
        return token.substring(0, delim);
    }
}