package com.worldlet.common.core.result;

import lombok.Getter;

/**
 * @author : Mr Li
 * @description : 全局排序字段枚举父类
 * @since : 2021/11/16 10:14 上午
 */
@Getter
public enum PageEnum {


    CREATED_TIME(1, "created_time"),
    ID(2, "id"),
    ;

    private final Integer type;
    private final String name;

    PageEnum(Integer type, String name) {
        this.type = type;
        this.name = name;
    }

    public static PageEnum getInstance(Integer type) {
        for (PageEnum value : PageEnum.values()) {
            if (type.equals(value.type)) {
                return value;
            }
        }
        return PageEnum.CREATED_TIME;
    }

}
