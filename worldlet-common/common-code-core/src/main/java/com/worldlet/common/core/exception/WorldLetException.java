package com.worldlet.common.core.exception;

import com.worldlet.common.core.constant.WorldletGlobalConstant;
import com.worldlet.common.core.enums.WorldletLanguageEnum;
import com.worldlet.common.core.result.RE;
import com.worldlet.common.core.util.WebUtil;
import lombok.Getter;

/**
 * @author : Li Hui
 * @description : 全局异常异常
 * @since : 2021/9/22 10:14 上午
 */
public class WorldLetException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    @Getter
    private final int code;

    public WorldLetException(RE re) {
        super(WorldletLanguageEnum.getMsg(WebUtil.getRequest().getHeader(WorldletGlobalConstant.LANGUAGE), re));
        this.code = re.getCode();
    }

    public WorldLetException(RE re, Object... parameters) {
        super(WorldletLanguageEnum.getMsg(WebUtil.getRequest().getHeader(WorldletGlobalConstant.LANGUAGE), re, parameters));
        this.code = RE.FAILED.getCode();
    }

    public WorldLetException(String msg, String englishMsg) {
        super(WorldletLanguageEnum.getMsg(WebUtil.getRequest().getHeader(WorldletGlobalConstant.LANGUAGE), msg, englishMsg));
        this.code = RE.FAILED.getCode();
    }

    public WorldLetException(int code, String msg, String englishMsg) {
        super(WorldletLanguageEnum.getMsg(WebUtil.getRequest().getHeader(WorldletGlobalConstant.LANGUAGE), msg, englishMsg));
        this.code = code;
    }
}
