package com.worldlet.common.core.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author : Li Hui
 * @description : 将对象拼接成Get路径头
 * @since : 2021/9/22 10:14 上午
 */
public class GetConventUtil {

    private GetConventUtil() {}

    /**
     * 使用 Map按key进行排序
     *
     * @param map f
     * @return f
     */
    public static Map<String, Object> sortMapByKey(Map<String, Object> map) {
        if (map == null || map.isEmpty()) {
            return Collections.emptyMap();
        }
        return new TreeMap<>(map);
    }

    /**
     * 签名
     */
    public static String sign(Map<String, Object> map) {
        map = sortMapByKey(map);
        List<Object> list = new ArrayList<>();
        var str = new StringBuilder();
        for (Map.Entry<String,Object> entry : map.entrySet()) {
            Object value = entry.getValue();
            if (null != value) {
                list.add(entry.getKey() + "=" + value);
            }
        }
        int leng = list.size() - 1;
        for (var i = 0; i < leng; i++) {
            str.append(list.get(i)).append("&");
            //拼索要加密的字符串格式
        }
        str.append(list.get(leng));
        return str.toString();
    }

}
