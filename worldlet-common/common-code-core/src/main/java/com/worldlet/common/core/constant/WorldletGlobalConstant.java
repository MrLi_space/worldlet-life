package com.worldlet.common.core.constant;

/**
 * @author : Mr Li
 * @description : 全局常量
 * @since : 2021/12/10 10:32 上午
 */
public class WorldletGlobalConstant {

    private WorldletGlobalConstant() {
    }

    public static final String LANGUAGE = "language";
}
