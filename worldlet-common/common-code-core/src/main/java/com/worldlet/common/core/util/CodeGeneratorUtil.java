package com.worldlet.common.core.util;

import cn.org.atool.generator.database.DbType;
import cn.org.atool.generator.FileGenerator;

import java.util.ArrayList;
import java.util.List;

/**
 * @author : Mr Li
 * @description : 代码生成类
 * @since : 2021/8/4 10:28 上午
 */
public class CodeGeneratorUtil {

    /**
     * @param dbType        数据库类型
     * @param driver        驱动
     * @param url           数据库地址
     * @param userName      数据库用户名
     * @param passWord      数据库密码
     * @param srcDir        设置具体的那个文件/哪个moudle的文件地址  =>  [....../src/main/java 所在的路径]
     * @param basePackage   设置项目全限包名 * cn.org.atool.fluent.mybatis.demo.
     * @param daoDir        设置dao接口的文件地址 src/main/java
     * @param tableNameList 表名称集合
     */
//    String dbType, String driver, String url, String userName, String passWord, String srcDir, String basePackage, String daoDir,
    public static void codeGenerator(List<String> tableNameList) {
        tableNameList.add("member");
        FileGenerator.build(true, false)
                .globalConfig(g -> g
                        .setOutputDir("worldlet-app-center")
                        .setBasePackage("src.main.java.com.worldlet.app")
                        .setDaoPackage("src.main.java.com.worldlet.app.infrastructure")
                        .setDataSource(DbType.getDbType("mysql"), "com.mysql.cj.jdbc.Driver", "jdbc:mysql://127.0.0.1:3306/worldlet?useUnicode=true&characterEncoding=utf8&useSSL=false&serverTimezone=GMT%2B8", "root", "root")
                )
                .tables(t -> tableNameList.forEach(t::table))
                .execute();
    }

    public static void main(String[] args) {
        CodeGeneratorUtil.codeGenerator(new ArrayList<>());
    }
}
