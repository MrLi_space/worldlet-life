package com.worldlet.common.core.util;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author : Li Hui
 * @description : 雪花ID
 * @since : 2021/9/22 10:14 上午
 */
public class SnowSlakeIDUtils extends IdUtil {

    private final Snowflake snowflake;

    private static final int SIZE = 32;

    private static class InnerClassHolder {
        private static final SnowSlakeIDUtils INSTANCE = new SnowSlakeIDUtils();
    }

    private SnowSlakeIDUtils() {
        this.snowflake = getSnowflake(getWorkerId(), getDataCenterId());
    }

    public static SnowSlakeIDUtils getInstance() {
        return InnerClassHolder.INSTANCE;
    }

    /**
     * 雪花算法生成ID
     *
     * @return 雪花ID
     */
    public Long getSnowflakeId() {
        return snowflake.nextId();
    }

    /**
     * 工作机器ID(0~31) 取机器ipv4地址和值对32求模
     *
     * @return 工作机器ID
     */
    private static Long getWorkerId() {
        try {
            String hostAddress = InetAddress.getLocalHost().getHostAddress();
            int[] ints = StringUtils.toCodePoints(hostAddress);
            int sums = 0;
            for (int b : ints) {
                sums += b;
            }
            return (long) (sums % SIZE);
        } catch (UnknownHostException e) {
            // 如果获取失败，则使用随机数备用
            return RandomUtils.nextLong(0, 31);
        }
    }

    /**
     * 数据中心ID(0~31) 取端口号对32求模
     *
     * @return 数据中心ID
     */
    private static Long getDataCenterId() {
        ApplicationContext applicationContext = SpringContextUtil.getApplicationContext();
        Environment environment = applicationContext.getEnvironment();
        Integer port = environment.getProperty("server.port", Integer.class);
        assert port != null;
        return (long) (port % SIZE);
    }
}