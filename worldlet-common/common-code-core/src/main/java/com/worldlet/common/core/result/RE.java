package com.worldlet.common.core.result;

import cn.hutool.http.HttpStatus;

/**
 * @author : Li Hui
 * @description : 全局状态枚举-中文
 * @since : 2021/9/22 10:14 上午
 */
public enum RE {
    /**
     * 操作成功！
     */
    OK(HttpStatus.HTTP_OK, "成功", "Success"),
    FAILED(HttpStatus.HTTP_INTERNAL_ERROR, "系统繁忙", "The system is busy"),
    FAILED_RELEASE(HttpStatus.HTTP_INTERNAL_ERROR, "请勿尝试释放其他线程资源锁","Do not try to release other thread resource locks"),
    FAILED_TOO_MANY_REQUESTS(HttpStatus.HTTP_INTERNAL_ERROR, "当前请求过多，请稍后重试", "There are too many requests currently, please try again later"),
    FAILED_REPEAT_REQUEST(HttpStatus.HTTP_INTERNAL_ERROR, "重复操作,请稍后尝试", "Repeat operation, please try again later"),
    FAILED_EMPTY(HttpStatus.HTTP_INTERNAL_ERROR, "指定数据不存在", "Specified data does not exist"),
    FAILED_REPEAT_PARAM_EMPTY(HttpStatus.HTTP_INTERNAL_ERROR, "请求参数 %s 为空", "Request parameter %s is empty"),
    FAILED_REPEAT_PARAM_INVALID(HttpStatus.HTTP_INTERNAL_ERROR, "无效参数 %s,可选值 %s", "Invalid parameter %s, optional value %s"),
    FAILED_REPEAT_FORBIDDEN(HttpStatus.HTTP_FORBIDDEN, "授权失败，禁止访问", "Authorization failed, access is forbidden"),
    FAILED_REQUEST_NOT_SUPPORTED(HttpStatus.HTTP_INTERNAL_ERROR, "请求方式不被允许", "Request method not allowed"),
    FAILED_REQUEST_LT_EQ_ZERO(HttpStatus.HTTP_INTERNAL_ERROR, "请求参数 %s 小于0", "Request parameter %s is less than 0"),
    FAILED_REQUEST_MIN_MAX(HttpStatus.HTTP_INTERNAL_ERROR, "请求参数 %s 必须在%s～%s范围内", "Request parameter %s must be in the range of %s～%s"),

    ;

    /**
     * 状态码
     */
    private final int code;

    /**
     * 消息
     */
    private final String msg;

    private final String englishMsg;

    RE(int code, String msg, String englishMsg) {
        this.code = code;
        this.msg = msg;
        this.englishMsg = englishMsg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public String getEnglishMsg() {
        return englishMsg;
    }
}
