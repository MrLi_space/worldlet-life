package com.worldlet.common.core.enums;

import cn.hutool.core.text.CharSequenceUtil;
import com.worldlet.common.core.result.RE;

import java.util.Locale;
import java.util.Objects;

/**
 * @author : Mr Li
 * @description : 系统常量
 * @since : 2021/11/19 2:38 下午
 */
public enum WorldletLanguageEnum {

    ZH("ZH", "英文"),
    CH("CH", "中文"),
    ;

    private final String language;

    private final String languageName;


    public String getLanguage() {
        return language;
    }

    public String getLanguageName() {
        return languageName;
    }

    WorldletLanguageEnum(String language, String languageName) {
        this.language = language;
        this.languageName = languageName;
    }

    public static WorldletLanguageEnum getInstance(String language) {
        if (CharSequenceUtil.isBlank(language)) {
            return CH;
        }
        language = language.toLowerCase(Locale.ROOT);
        for (WorldletLanguageEnum value : WorldletLanguageEnum.values()) {
            if (Objects.equals(value.language, language)) {
                return value;
            }
        }
        return CH;
    }

    public static String getMsg(String language, RE re) {
        if (CharSequenceUtil.isBlank(language)) {
            return re.getMsg();
        }
        language = language.toLowerCase(Locale.ROOT);
        if (language.equals(ZH.language)) {
            return re.getEnglishMsg();
        }
        return re.getMsg();
    }

    public static String getMsg(String language, RE re,Object... parameters) {
        if (CharSequenceUtil.isBlank(language)) {
            return String.format(re.getMsg(),parameters);
        }
        language = language.toLowerCase(Locale.ROOT);
        if (language.equals(ZH.language)) {
            return String.format(re.getEnglishMsg(),parameters);
        }
        return re.getMsg();
    }

    public static String getMsg(String language, String msg, String englishMsg) {
        if (CharSequenceUtil.isBlank(language)) {
            return msg;
        }
        language = language.toLowerCase(Locale.ROOT);
        if (language.equals(ZH.language)) {
            return englishMsg;
        }
        return msg;
    }


}
