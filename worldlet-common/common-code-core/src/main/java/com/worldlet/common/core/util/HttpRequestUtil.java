package com.worldlet.common.core.util;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author : Mr Li
 * @description : Http请求工具
 * @since : 2021/11/23 9:08 上午
 */
public class HttpRequestUtil {

    private HttpRequestUtil() {
    }

    public static Map<String, Object> convertBodyToMap(HttpServletRequest req) {
        final String requestPayload = getRequestPayload(req);
        Map<String, Object> map = new HashMap<>();
        strRequest(map, requestPayload);
        return map;
    }

    private static String getRequestPayload(HttpServletRequest req) {
        StringBuilder sb = new StringBuilder();
        try (BufferedReader reader = req.getReader()) {
            char[] buff = new char[1024];
            int len;
            while ((len = reader.read(buff)) != -1) {
                sb.append(buff, 0, len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    private static String strRequest(Map<String, Object> map, String s) {
        int index1 = s.indexOf("=");
        String param1 = s.substring(0, index1);
        int index2 = s.indexOf("&");
        if (index2 == -1) {
            String param2 = s.substring(index1 + 1);
            map.put(param1, param2);
            return null;
        }
        Object par2 = s.substring(index1 + 1, index2);
        map.put(param1, par2);
        return strRequest(map, s.substring(index2 + 1));
    }


}
