package com.worldlet.common.rocketmq.exception;

import com.worldlet.common.core.exception.WorldLetException;
import com.worldlet.common.core.result.RE;

/**
 * @author : Mr Li
 * @description : RocketMQ服务异常类
 * @since : 2021/9/30 9:36 上午
 */
public class RocketMQException extends WorldLetException {

    public RocketMQException(RE re) {
        super(re);
    }

    public RocketMQException(RE re, Object... parameters) {
        super(re, parameters);
    }

    public RocketMQException(String msg, String englishMsg) {
        super(msg, englishMsg);
    }

    public RocketMQException(int code, String msg, String englishMsg) {
        super(code, msg, englishMsg);
    }

}
