package com.worldlet.common.rabbitmq.enums;

import com.worldlet.common.rabbitmq.exception.RabbitMQException;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author : Mr Li
 * @description : RABBITMQ 异常枚举类
 * @since : 2021/9/30 9:38 上午
 */
@Slf4j
public enum RabbitMQExceptionEnum {

    RABBITMQ_SERVER_FAILED(500210, "RabbitMQ服务器异常-%s", "RabbitMQ Server abnormal-%s"),

    RABBITMQ_CLIENT_ERROR(500211, "客户端初始化异常-%s", "Client initialization exception-%s"),

    RABBITMQ_CLIENT_INIT_ERROR(500212, "客户端初始化异常-%s", "Client initialization exception-%s"),

    RABBITMQ_SEND_MSG_ERROR(500213, "RabbitMQ发送消息失败-correlationData(%s),cause(%s)", "RabbitMQ send message failed-correlationData(%s),cause(%s)"),

    RABBITMQ_MSG_LOST(500213, "RabbitMQ消息丢失-exchange-%s-route-%s-replyCode-%s-replyText-%s-message-%s", "RabbitMQ messages are lost-exchange-%s-route-%s-replyCode-%s-replyText-%s-message-%s"),

    ;

    @Getter
    private final int code;

    @Getter
    private final String msg;

    @Getter
    private final String englishMsg;

    RabbitMQExceptionEnum(int code, String msg, String englishMsg) {
        this.code = code;
        this.msg = msg;
        this.englishMsg = englishMsg;
    }

    public static RabbitMQExceptionEnum getInstance(int code) {
        for (RabbitMQExceptionEnum value : RabbitMQExceptionEnum.values()) {
            if (value.code == code)
                return value;
        }
        log.error("ERROR-无{}对应存储方式", code);
        throw new RabbitMQException(RabbitMQExceptionEnum.RABBITMQ_CLIENT_ERROR, code);
    }

}
