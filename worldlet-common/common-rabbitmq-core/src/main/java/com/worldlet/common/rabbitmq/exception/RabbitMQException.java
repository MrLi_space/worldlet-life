package com.worldlet.common.rabbitmq.exception;

import com.worldlet.common.core.exception.WorldLetException;
import com.worldlet.common.core.result.RE;
import com.worldlet.common.rabbitmq.enums.RabbitMQExceptionEnum;

/**
 * @author : Mr Li
 * @description : RabbitMQ服务异常类
 * @since : 2021/9/30 9:36 上午
 */
public class RabbitMQException extends WorldLetException {

    public RabbitMQException(RE re) {
        super(re);
    }

    public RabbitMQException(RabbitMQExceptionEnum rabbitMQExceptionEnum) {
        super(rabbitMQExceptionEnum.getMsg(), rabbitMQExceptionEnum.getEnglishMsg());
    }

    public RabbitMQException(RabbitMQExceptionEnum rabbitMQExceptionEnum, Object... parameters) {
        super(rabbitMQExceptionEnum.getCode(), String.format(rabbitMQExceptionEnum.getMsg(), parameters),
                String.format(rabbitMQExceptionEnum.getEnglishMsg(), parameters));
    }
}
