package com.worldlet.common.rabbitmq.enums;

import lombok.Getter;

/**
 * @author : Li Hui
 * @description : rabbitMQ枚举
 * @since : 2021/7/5 11:36 上午
 */
public enum RabbitMQEnum {

    TEST_MQ("test_exchange", "com.dream.music", "test_queue2"),
    TEST_TOPIC_MQ("exchange_topic", "com.dream.music", "test_queue1"),
    TEST_FANOUT_MQ("exchange_fanout", "test_routkey", "test_queue");


    @Getter
    private final String exchange;

    @Getter
    private final String routKey;

    @Getter
    private final String queue;

    RabbitMQEnum(String exchange, String routKey, String queue) {
        this.exchange = exchange;
        this.routKey = routKey;
        this.queue = queue;
    }

}
