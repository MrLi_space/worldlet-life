package com.worldlet.common.rabbitmq.config;

import com.worldlet.common.rabbitmq.exception.RabbitMQException;
import com.worldlet.common.rabbitmq.enums.RabbitMQExceptionEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import javax.annotation.Resource;

/**
 * @author : Li Hui
 * @description : rabbitMQ配置
 * @since : 2021/7/5 10:00 上午
 */
@Slf4j
@Configuration
public class RabbitMQConfig {

    @Resource
    private CachingConnectionFactory connectionFactory;

    @Bean
    public RabbitTemplate rabbitTemplate() {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        // 如果消息没有到exchange,则confirm回调,ack=false; 如果消息到达exchange,则confirm回调,ack=true
        rabbitTemplate.setConfirmCallback((correlationData, ack, cause) -> {
            if (!ack) {
                log.error("消息发送失败:correlationData({}),cause({})", correlationData, cause);
                throw new RabbitMQException(RabbitMQExceptionEnum.RABBITMQ_SEND_MSG_ERROR,correlationData, cause);
            }
        });
        //如果exchange到queue成功,则不回调return;如果exchange到queue失败,则回调return(需设置mandatory=true,否则不回回调,消息就丢了)
        rabbitTemplate.setReturnCallback((message, replyCode, replyText, exchange, routingKey) -> {
            log.error("消息丢失:exchange({}),route({}),replyCode({}),replyText({}),message:{}",
                    exchange, routingKey, replyCode, replyText, message);
            throw new RabbitMQException(RabbitMQExceptionEnum.RABBITMQ_MSG_LOST,exchange, routingKey, replyCode, replyText, message);
        });
        return rabbitTemplate;
    }

}
