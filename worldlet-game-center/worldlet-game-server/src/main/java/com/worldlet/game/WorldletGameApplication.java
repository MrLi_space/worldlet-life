package com.worldlet.game;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author : Mr Li
 * @description : 游戏主启动类
 * @since : 2021/12/17 2:38 下午
 */
@SpringBootApplication
public class WorldletGameApplication {
    public static void main(String[] args) {
        SpringApplication.run(WorldletGameApplication.class, args);
    }
}
