package com.worldlet.system;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author : Mr Li
 * @description : 系统主启动类
 * @since : 2021/12/17 2:48 下午
 */
@SpringBootApplication
public class WorldletSystemApplication {
    public static void main(String[] args) {
        SpringApplication.run(WorldletSystemApplication.class, args);
    }
}
