package com.worldlet.im;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author : Mr Li
 * @description : IM服务主启动类
 * @since : 2021/12/17 3:38 下午
 */
@SpringBootApplication
public class WorldletIMApplication {
    public static void main(String[] args) {
        SpringApplication.run(WorldletIMApplication.class, args);
    }
}
