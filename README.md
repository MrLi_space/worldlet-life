
# [小世界-Worldlet](https://www.worldlet.life)<img alt="小世界" height="32" src="https://worldlet-life.oss-cn-beijing.aliyuncs.com/worldlet.ico" width="32"/>

## 项目介绍

A Dream Interstellar Space Of Yourself Build Your Own Small World Love Life,Enjoy Life

### 系统分布图

![系统分布图](https://worldlet-life.oss-cn-beijing.aliyuncs.com/worldlet-system-architecture.png)

#### 项目结构采用DDD结构分层模式,将功能与业务拆分的更细,减少相应的耦合度,增加对应的内聚

#### 代码规范IDEA插件-SonarLint

#### 代码文档一律采用规范注释-上传![Yapi](https://hellosean1025.github.io/yapi/documents/images/logo_header@2x.png) [相关文档信息](https://hellosean1025.github.io/yapi/)

#### 系统运行环境-JDK-11

#### 单独项目服务创建子模版DDD-Module

    module-system-interface      【系统对外暴露的接口层(Controller、VO、DTO)层】等同于controller包

    module-system-application    【对应到系统用例层(Service接口)层】service不包含impl

    module-system-domain         【核心业务逻辑层(Entity/DO、ServiceImpl、抽象、策略)层】实体+impl+业务

    module-system-infrastructure 【负责所有的对外的交互(Mapper/Dao、RPC接口、MQ、Common包)层】dao+xml+feignApi+common

#### 架构中间件选型

**MQ-集群**

    RabbitMQ-支付

    RocketMQ-消息

**DB-集群**

    Mysql-除IM消息外的所有数据,底层DB

    ElasticSearch-搜索引擎

    MongoDB-消息

    Redis-缓存

**DB工具、中间件**

    Canel-数据同步

    Sharding-Sphere-分库分表

**其他中间价**

    XXl-Job-定时任务

    Redisson-分布式锁
    
    Netty-IM框架

    seata

**微服务生态**

    GateWay

    Nacos

    OpenFeign

    Rabbion

    Zikin、Sleuth

**项目代码规则**
<h3 id="1.1">1.1-自定义状态码及其内容支持国际化</h3>
    OK(HttpStatus.HTTP_OK, "成功", "Success")


    

    
    
    









