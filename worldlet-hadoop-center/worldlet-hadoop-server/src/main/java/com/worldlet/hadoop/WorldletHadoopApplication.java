package com.worldlet.hadoop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author : Mr Li
 * @description : 大数据主启动类
 * @since : 2021/12/17 2:43 下午
 */
@SpringBootApplication
public class WorldletHadoopApplication {
    public static void main(String[] args) {
        SpringApplication.run(WorldletHadoopApplication.class, args);
    }
}
