package com.worldlet.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author : Mr Li
 * @description : APP启动类
 * @since : 2021/11/25 2:00 下午
 */
@SpringBootApplication
public class WorldletApplication {
    public static void main(String[] args) {
        SpringApplication.run(WorldletApplication.class, args);
    }
}
