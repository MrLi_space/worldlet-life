package com.worldlet.operation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author : Mr Li
 * @description : 运营主启动类
 * @since : 2021/12/17 3:44 下午
 */
@SpringBootApplication
public class WorldletOperationApplication {

    public static void main(String[] args) {
        SpringApplication.run(WorldletOperationApplication.class, args);
    }
}
