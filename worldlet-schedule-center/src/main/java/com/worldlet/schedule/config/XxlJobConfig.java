package com.worldlet.schedule.config;

import com.xxl.job.core.executor.impl.XxlJobSpringExecutor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author ：Li Hui
 * @version ：1.0
 * @description ：xxl-job配置类
 * @program ：schedule-center
 * @date ：Created in 2021/4/9 10:40
 */
@Slf4j
@Configuration
public class XxlJobConfig {

    @Bean
    public XxlJobSpringExecutor xxlJobExecutor() {
        log.info(">>>>>>>>>>> xxl-job config init.");
        XxlJobSpringExecutor xxlJobSpringExecutor = new XxlJobSpringExecutor();
        xxlJobSpringExecutor.setAdminAddresses("http://47.106.213.1:9999/xxl-job-admin");
        xxlJobSpringExecutor.setAppname("schedule-order-task-server");
        xxlJobSpringExecutor.setPort(9900);
        xxlJobSpringExecutor.setAccessToken("token");
        xxlJobSpringExecutor.setLogPath("/Users/zeek/logs/");
        xxlJobSpringExecutor.setLogRetentionDays(1);
        return xxlJobSpringExecutor;
    }

}