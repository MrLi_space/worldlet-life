package com.worldlet.schedule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author : Mr Li
 * @description : 定时任务启动累
 * @since : 2021/9/29 2:38 下午
 */
@EnableFeignClients(basePackages = {"com.worldlet.**.api"})
@SpringBootApplication
public class WorldletScheduleApplication {
    public static void main(String[] args) {
        SpringApplication.run(WorldletScheduleApplication.class,args);
    }
}
