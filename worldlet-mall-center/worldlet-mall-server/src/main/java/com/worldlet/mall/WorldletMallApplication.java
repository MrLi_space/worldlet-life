package com.worldlet.mall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author : Mr Li
 * @description : 商城主启动类
 * @since : 2021/12/17 3:46 下午
 */
@SpringBootApplication
public class WorldletMallApplication {
    public static void main(String[] args) {
        SpringApplication.run(WorldletMallApplication.class, args);
    }
}
